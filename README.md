# Cliente ChatGPT

Una aplicación sencilla hecha con Java y Springboot para conectarse a ChatGPT.

## Requerimientos

- Java 17
- Maven 3+
- API Key de OpenAI<sup>1</sup>

<sup>1</sup> Para obtener una llave debes crear o usar una cuenta de [OpenAI](https://platform.openai.com). Luego, accede a la opción [API Keys](https://platform.openai.com/account/api-keys) y crear una. Trátala con cuidado, al generarse solo la podrás ver 1 sola vez.

## Ejecución

1. Abrir el archivo `src/main/resources/application.properties` y reemplazar el valor de la propiedad `chatgpt.api.key`.
1. Compilar el proyecto. Ejecutar la aplicación con la clase `edu.ltmj.chatgptdemo.App` como punto de entrada.
1. Ejecutar una llamada HTTP al endpoint `localhost:8080/chats`. Ejemplo:
    ```
    curl localhost:8080/chats \
    -d '{
      "messages": [
        "Hola ChatGPT. ¿Cómo estás?"
      ],
      "user": "Tu nombre"
    }'
    ```

## Autor

Luiggi Mendoza

## DISCLAIMER

Este es un proyecto meramente de prueba. El uso y aplicabilidad del código quedan totalmente en manos de los usuarios. Me libero de responsabilidad de cualquier problema ocasionado por el mal uso de este código.
