package edu.ltmj.chatgptdemo.controller;

import edu.ltmj.chatgptdemo.model.CompletionResponse;
import edu.ltmj.chatgptdemo.model.CompletionRequest;
import edu.ltmj.chatgptdemo.service.ChatService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/chats")
public class ChatController {

    private final ChatService chatService;

    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @PostMapping(value={"", "/"})
    public CompletionResponse chatWithMe(@Validated @RequestBody CompletionRequest request) {
        return chatService.chat(request);
    }
}
