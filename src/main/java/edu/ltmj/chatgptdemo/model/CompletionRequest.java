package edu.ltmj.chatgptdemo.model;

import java.util.List;

public record CompletionRequest(List<String> messages, String user) {
}
