package edu.ltmj.chatgptdemo.model;

import java.util.List;

public record CompletionResponse(List<String> chatResponses) {
}
