package edu.ltmj.chatgptdemo.repository;

import java.util.List;

public record ChatCompletionMessage(String role, String content) {

}
