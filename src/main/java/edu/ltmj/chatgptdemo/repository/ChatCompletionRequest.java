package edu.ltmj.chatgptdemo.repository;

import java.util.List;

public record ChatCompletionRequest(List<ChatCompletionMessage> messages, String user, String model){
}
