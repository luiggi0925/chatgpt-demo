package edu.ltmj.chatgptdemo.repository;

import edu.ltmj.chatgptdemo.model.CompletionRequest;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ChatGPTClient {

    private static final String model = "gpt-3.5-turbo";
    private static final String OPENAI_HOST = "https://api.openai.com";
    private static final String CHAT_COMPLETIONS_URL = "/v1/chat/completions";
    private static final String USER_ROLE = "user";

    private final RestTemplate restTemplate;

    public ChatGPTClient(@Qualifier("chatGPTRestTemplate") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ChatCompletionResponse chatCompletion(CompletionRequest completionRequest) {
        ChatCompletionRequest request = new ChatCompletionRequest(
            completionRequest.messages().stream()
                .map(message -> new ChatCompletionMessage(USER_ROLE, message))
                .collect(Collectors.toList()),
            completionRequest.user(),
            model);
        ResponseEntity<ChatCompletionResponse> response = restTemplate
            .postForEntity(
                OPENAI_HOST + CHAT_COMPLETIONS_URL,
                request,
                ChatCompletionResponse.class);
        return response.getBody();
    }
    /*
    {
        "error": {
            "message": "'Hello world. How are you doing?' is not of type 'object' - 'messages.0'",
            "type": "invalid_request_error",
            "param": null,
            "code": null
        }
    }
     */
}
