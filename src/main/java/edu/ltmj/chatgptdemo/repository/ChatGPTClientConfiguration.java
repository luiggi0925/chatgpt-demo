package edu.ltmj.chatgptdemo.repository;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ChatGPTClientConfiguration {

    private static final String CONTENT_TYPE_HEADER_NAME = "Content-Type";
    private static final String CONTENT_TYPE_HEADER_VALUE = "application/json";
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    @Value("${chatgpt.api.key}")
    private String chatGPTApiKey;
    @Value("${chatgpt.connection.timeout}")
    private int chatGPTConnectionTimeout;

    @Bean
    public RestTemplate chatGPTRestTemplate(RestTemplateBuilder builder) {
        return builder
            .setConnectTimeout(Duration.of(chatGPTConnectionTimeout, ChronoUnit.SECONDS))
            .additionalInterceptors( (request, body, execution) -> {
                request.getHeaders().add(CONTENT_TYPE_HEADER_NAME, CONTENT_TYPE_HEADER_VALUE);
                request.getHeaders().add(AUTHORIZATION_HEADER_NAME, String.format("Bearer %s", chatGPTApiKey));
                return execution.execute(request, body);
            }).build();
    }
}
