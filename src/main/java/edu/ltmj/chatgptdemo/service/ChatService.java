package edu.ltmj.chatgptdemo.service;

import edu.ltmj.chatgptdemo.model.CompletionResponse;
import edu.ltmj.chatgptdemo.repository.ChatCompletionResponse;
import edu.ltmj.chatgptdemo.model.CompletionRequest;
import edu.ltmj.chatgptdemo.repository.ChatGPTClient;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class ChatService {
    private final ChatGPTClient chatGPTClient;
    public ChatService(ChatGPTClient chatGPTClient) {
        this.chatGPTClient = chatGPTClient;
    }

    public CompletionResponse chat(CompletionRequest request) {
        ChatCompletionResponse response = chatGPTClient.chatCompletion(request);
        //TODO almacenar el historial de los mensajes en alguna fuente de datos
        //TODO almacenar el historial de tokens utilizados
        return new CompletionResponse(
            response.getChoices().stream()
                .map(choice -> choice.getMessage().getContent())
                .collect(Collectors.toList())
        );
    }
}
